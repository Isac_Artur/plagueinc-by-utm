//
// Created by fox on 2/24/20.
//

#include "SIR_model.h"


void sir( const state_type &x , state_type &dxdt , double t ){
    dxdt[0] = -b * x[0] * x[1];
    dxdt[1] = b * x[0] * x[1] - a * x[1];
    dxdt[2] = a * x[1];
}

void write_sir(const state_type &x , const double t ){
    printf("%f\t%.10lf\t%.10lf\t%.10lf\t", t, x[0], x[1], x[2]);
}