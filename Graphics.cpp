//
// Created by fox on 2/24/20.
//

#include <iostream>
#include "Graphics.h"

int offsetPos_y = 0, selectedCountryID = -1;

bool startClick = 0;


//----------------------------------------------------------------------------------------------------------------------
// MAIN SCREEN OBJECT
//----------------------------------------------------------------------------------------------------------------------
MainScreen::MainScreen(RenderWindow &current_window) {
    if (!loadingName_i.loadFromFile("source/sprites/name.png"))
        printf("Something went wrong on loading logo\n");
    if (!loadingLogo_i.loadFromFile("source/sprites/logo.png"))
        printf("Something went wrong on loading name\n");

    loadingName_t.loadFromImage(loadingName_i);
    loadingLogo_t.loadFromImage(loadingLogo_i);
    loadingLogo_t.setSmooth(true);

    loadingScreenLogo_s.setTexture(loadingLogo_t);
    loadingScreenLogo_s.setOrigin(LOGO_SIZE_X / 2.f, LOGO_SIZE_Y / 2.f);
    loadingScreenLogo_s.setPosition(current_window.getSize().x / 2, current_window.getSize().y / 4);

    loadingScreenName_s.setTexture(loadingName_t);
    loadingScreenName_s.setOrigin(LOGO_NAME_SIZE_X / 2, LOGO_NAME_SIZE_Y / 2);
    loadingScreenName_s.setPosition(loadingScreenLogo_s.getPosition());
    loadingScreenName_s.move(0, 300);

    offsetPos_y = loadingScreenName_s.getPosition().y;

    nameFade = 0, logoFade = 0;
}

void MainScreen::updateVariables(double timePerFrame, double timeElapsed, RenderWindow &inputScreen) {
    loadingScreenLogo_s.rotate(0.02 * timePerFrame);
    loadingScreenName_s.setColor(Color(255, 255, 255, nameFade));
    loadingScreenLogo_s.setColor(Color(255, 255, 255, logoFade));

    if (timeElapsed >= 2.5 && loadingScreenLogo_s.getScale().x > 0.3) {
        double shrinkCoefficient = 1 - 0.002 * timePerFrame;
        loadingScreenLogo_s.scale(shrinkCoefficient,
                                  shrinkCoefficient);
        loadingScreenLogo_s.move(-0.75 * timePerFrame, 0);
        loadingScreenName_s.move(0, -0.49 * timePerFrame);
    }

//    else if(timeElapsed>3.3){
//
//    }

    draw(inputScreen);

    if (nameFade > 60 && logoFade < 252 && !startClick)logoFade += 0.1 * timePerFrame;
    if (nameFade < 253 && !startClick)nameFade += 0.1 * timePerFrame;
    else if (startClick && nameFade >= 2) {
        nameFade -= 0.2 * timePerFrame;
        logoFade -= 0.2 * timePerFrame;
    }
}

void MainScreen::draw(RenderWindow &inputScreen) {
    inputScreen.draw(loadingScreenLogo_s);
    inputScreen.draw(loadingScreenName_s);
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------------
// MAIN SCREEN BUTTONS
//----------------------------------------------------------------------------------------------------------------------

MenuButtons::MenuButtons(RenderWindow &current_window) {
    int window_x = current_window.getSize().x;

    if (!square_i.loadFromFile("source/sprites/Square.png"))
        printf("Loading square image failed!\n");
    if (!door_i.loadFromFile("source/sprites/Door.png"))
        printf("Loading door image failed!\n");
    if (!folder_i.loadFromFile("source/sprites/Folder.png"))
        printf("Loading folder image failed!\n");
    if (!gear_i.loadFromFile("source/sprites/Gear_r.png"))
        printf("Loading gear image failed!\n");
    if (!joystick_i.loadFromFile("source/sprites/Joy.png"))
        printf("Loading folder image failed!\n");

    square_t.loadFromImage(square_i);
    square_t.setSmooth(true);

    door_t.loadFromImage(door_i);
    door_t.setSmooth(true);

    folder_t.loadFromImage(folder_i);
    folder_t.setSmooth(true);

    gear_t.loadFromImage(gear_i);
    gear_t.setSmooth(true);

    joystick_t.loadFromImage(joystick_i);
    joystick_t.setSmooth(true);

    for (int i = 0; i < 4; i++) {
        square_s[i].setTexture(square_t);
        square_s[i].setOrigin(dimension_size / 2, dimension_size / 2);
    }

    door_s.setTexture(door_t);
    door_s.setOrigin(dimension_size / 2, dimension_size / 2);

    folder_s.setTexture(folder_t);
    folder_s.setOrigin(dimension_size / 2, dimension_size / 2);

    gear_s[0].setTexture(gear_t);
    gear_s[0].setOrigin(dimension_size / 2, dimension_size / 2);
    gear_s[0].setScale(gear_scale_main, gear_scale_main);

    gear_s[1].setTexture(gear_t);
    gear_s[1].setOrigin(dimension_size / 2, dimension_size / 2);
    gear_s[1].rotate(23);
    gear_s[1].setScale(gear_scale_factor * gear_scale_main, gear_scale_factor * gear_scale_main);

    joystick_s.setTexture(joystick_t);
    joystick_s.setOrigin(dimension_size / 2, dimension_size / 2);


    int offsetLocal_y = offsetPos_y + 50;
    startClick = 0;
    timeOff = 0;


    square_s[0].setPosition((window_x / 2 - dimension_size / 2) + square_size,
                            (offsetLocal_y - dimension_size / 2) + square_size);

    square_s[1].setPosition((window_x / 2 - dimension_size / 2) + square_size,
                            (offsetLocal_y + dimension_size / 2) - square_size);

    square_s[2].setPosition((window_x / 2 + dimension_size / 2) - square_size,
                            (offsetLocal_y - dimension_size / 2) + square_size);

    square_s[3].setPosition((window_x / 2 + dimension_size / 2) - square_size,
                            (offsetLocal_y + dimension_size / 2) - square_size);


    gear_s[0].setPosition((window_x / 2 - dimension_size / 2) + square_size - gear_offset,
                          (offsetLocal_y + dimension_size / 2) - square_size + gear_offset);
    gear_s[1].setPosition((window_x / 2 - dimension_size / 2) + square_size + gear_offset,
                          (offsetLocal_y + dimension_size / 2) - square_size - gear_offset);


    door_s.setPosition((window_x / 2 + dimension_size / 2) - square_size,
                       (offsetLocal_y + dimension_size / 2) - square_size);

    folder_s.setPosition((window_x / 2 + dimension_size / 2) - square_size,
                         (offsetLocal_y - dimension_size / 2) + square_size);

    joystick_s.setPosition((window_x / 2 - dimension_size / 2) + square_size,
                           (offsetLocal_y - dimension_size / 2) + square_size);

    gear_velocity = 0.07, mainFade = 0;
}

void MenuButtons::draw(RenderWindow &current_window) {
    for (int i = 0; i < 4; i++)
        current_window.draw(square_s[i]);

    for (int i = 0; i < 2; i++)
        current_window.draw(gear_s[i]);

    current_window.draw(door_s);
    current_window.draw(folder_s);
    current_window.draw(joystick_s);
}

int MenuButtons::updateVariables(double timePerFrame, double timeElapsed, RenderWindow &current_window) {
    if (square_s[1].getGlobalBounds().contains(current_window.mapPixelToCoords(Mouse::getPosition(current_window)))) {
        gear_s[0].rotate(gear_velocity * timePerFrame);
        gear_s[1].rotate(-gear_velocity * timePerFrame);
    } else if (square_s[0].getGlobalBounds().contains(
            current_window.mapPixelToCoords(Mouse::getPosition(current_window)))) {
        if (Mouse::isButtonPressed(Mouse::Button::Left)) {
            timeOff = timeElapsed;
            startClick = 1;
        }
    }

    for (int i = 0; i < 4; i++)
        square_s[i].setColor(Color(255, 255, 255, mainFade));
    for (int i = 0; i < 2; i++)
        gear_s[i].setColor(Color(255, 255, 255, mainFade));

    joystick_s.setColor(Color(255, 255, 255, mainFade));
    folder_s.setColor(Color(255, 255, 255, mainFade));
    door_s.setColor(Color(255, 255, 255, mainFade));

    if (mainFade < 254 && !startClick)mainFade += 0.2 * timePerFrame;
    if (startClick && mainFade >= 2)mainFade -= 2 * timePerFrame;
    if (startClick && timeElapsed - timeOff > 1.5)return 1;

    return 0;
}

int MenuButtons::process(double timePerFrame, double timeElapsed, RenderWindow &current_window) {
    int out = updateVariables(timePerFrame, timeElapsed, current_window);
    draw(current_window);
    return out;
}

void MenuButtons::offset(double x, double y) {
    for (int i = 0; i < 4; i++)
        square_s[i].move(x, y);

    for (int i = 0; i < 2; i++)
        gear_s[i].move(x, y);

    joystick_s.move(x, y);
    folder_s.move(x, y);
    door_s.move(x, y);
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------
// WORLD MAP
//----------------------------------------------------------------------------------------------------------------------

WorldMap::WorldMap(RenderWindow &current_window) {
    if (!map_mask.loadFromFile("source/sprites/map_mask_color.png"))
        printf("Error loading map mask!\n");
    if (!map_real_i.loadFromFile("source/sprites/map_landscape.png"))
        printf("Error loading map landscape!\n");
    if (!map_outline_i.loadFromFile("source/sprites/map_mask.png"))
        printf("Error loading map outline!\n");

    mapButtonOff = 0;
    mapClickOff = 0;

    map_real_t.loadFromImage(map_real_i);
    map_real_t.setSmooth(true);
    map_mask_t.loadFromImage(map_mask);
    map_outline_t.loadFromImage(map_outline_i);

    map_real_s.setTexture(map_real_t);
    map_mask_s.setTexture(map_mask_t);
    map_outline_s.setTexture(map_outline_t);


    map_real_s.setScale(MAP_SCALE_FACTOR, MAP_SCALE_FACTOR);
    map_mask_s.setScale(MAP_SCALE_FACTOR, MAP_SCALE_FACTOR);
    map_outline_s.setScale(MAP_SCALE_FACTOR, MAP_SCALE_FACTOR);

    mapMode = true;
}

void WorldMap::updateVariables(double timePerFrame, double timeElapsed, RenderWindow &current_window) {
    draw(current_window);
    Vector2i mousePos = Mouse::getPosition(current_window);
    if (Mouse::isButtonPressed(Mouse::Button::Left) && timeElapsed - mapClickOff > 0.2 && mousePos.x>0
      && mousePos.y > 0 && mousePos.x < current_window.getSize().x && mousePos.y < current_window.getSize().y) {
        Color buffer = map_mask.getPixel(
                Mouse::getPosition(current_window).x / MAP_SCALE_FACTOR,
                Mouse::getPosition(current_window).y / MAP_SCALE_FACTOR);

        int green = buffer.g;
        int red = buffer.r;
        mapClickOff = timeElapsed;

        if (red == 100) {
            selectedCountryID = (green - 100) / 2;
        } else if (red == 200) {
            selectedCountryID = (green - 100) / 2 + 78;
        }
//        else
//            std::cout << "Wrong coordinates\n";
    }
    if (Keyboard::isKeyPressed(Keyboard::Key::M) && timeElapsed - mapButtonOff > 0.5) {
        mapMode = !mapMode;
        mapButtonOff = timeElapsed;
    }
}

void WorldMap::draw(RenderWindow &current_window) {
    current_window.draw(map_real_s);
    if (mapMode)current_window.draw(map_outline_s);
    else current_window.draw(map_mask_s);
}


//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// GUI
//----------------------------------------------------------------------------------------------------------------------

Gui::Gui(RenderWindow &current_window) {
    statusBar_i.loadFromFile("source/sprites/gui_1.png");
    statusBar_t.loadFromImage(statusBar_i);
    statusBar_s.setTexture(statusBar_t);
    statusBar_s.setPosition(0, current_window.getSize().y - statusBar_s.getGlobalBounds().height);

    InfoBar_i.loadFromFile("source/sprites/gui_2.png");
    InfoBar_t.loadFromImage(InfoBar_i);

    for (int i = 0; i < BAR_QUANTITY; i++) {
        InfoBar_s[i].setTexture(InfoBar_t);
        if (i == 0)
            InfoBar_s[i].setPosition(current_window.getSize().x - InfoBar_s[0].getGlobalBounds().left -
                                     InfoBar_s[0].getGlobalBounds().width, 0);
        else
            InfoBar_s[i].setPosition(InfoBar_s[0].getPosition().x,
                                     InfoBar_s[i - 1].getPosition().y + InfoBar_s[0].getGlobalBounds().height);
    }

    Info_f.loadFromFile("source/fonts/font1.ttf");

    for (int i = 0; i < INFO_QUANTITY; i++) {
        Info_t[i].setFont(Info_f);
        Info_t[i].setCharacterSize(20);
        Info_t[i].setPosition(InfoBar_s[0].getPosition().x + STRINGS_OFFSET_X,
                              InfoBar_s[i / 5].getPosition().y + (STRINGS_OFFSET_Y * (i % 5)) + 10);
        Info_t[i].setStyle(Text::Bold);
    }

    selectedCountry = "";
    tempCountryID = selectedCountryID;

    Info_t[10].setString("Some stats:");
    Info_t[11].setString("Nice dick:");
    Info_t[12].setString("Awesome cock:");

    statusText_t.setFont(Info_f);
    statusText_t.setString("Choose country to initiate pandemia!");
    statusText_t.setCharacterSize(45);
    statusText_t.setOrigin(statusText_t.getGlobalBounds().width / 2, statusText_t.getGlobalBounds().height / 2);
    statusText_t.setPosition(current_window.getSize().x / 2,
                             statusBar_s.getPosition().y + statusBar_s.getGlobalBounds().height / 2);
    statusText_t.setStyle(Text::Bold);

    Info_t[0].setString("Local ()");
    Info_t[1].setString("Suspected:\n0");
    Info_t[2].setString("Infected:\n0");
    Info_t[3].setString("Recovered:\n0");
    Info_t[4].setString("Dead:\n0");
    Info_t[5].setString("Global:");
    Info_t[6].setString("Suspected:\n0");
    Info_t[7].setString("Infected:\n0");
    Info_t[8].setString("Recovered:\n0");
    Info_t[9].setString("Dead:\n0");

}

void Gui::draw(RenderWindow &current_window) {
    for (int i = 0; i < BAR_QUANTITY; i++)
        current_window.draw(InfoBar_s[i]);

    for (int i = 0; i < INFO_QUANTITY; i++)
        current_window.draw(Info_t[i]);

    current_window.draw(statusBar_s);
    current_window.draw(statusText_t);
}

void Gui::updateVariables(double timeElapsed, RenderWindow &current_window, DataFrame df) {

    if (selectedCountryID != -1 && tempCountryID != selectedCountryID) {
        tempCountryID = selectedCountryID;
        printf("%d\t", selectedCountryID);
        selectedCountry = df.getData(selectedCountryID, 0);

        Info_t[0].setString("Local (" + selectedCountry + "):");
        Info_t[1].setString("Suspected:\n" + df.getData(selectedCountryID, 1));
    }


    draw(current_window);
}
// 100 RED / 100 BLUE / VAR GREEN
/*
 *100 - russia
 *102 - mongolia
 *104 - china
 *106 - burma
 *108 - laos
 *110 - thailand
 *112 - cambodia
 *114 - vietnam
 *116 - indonesia
 *118 - malaysia
 *120 - australia
 *122 - new zealand
 *124 - nepal
 *126 - india
 *128 - bangl
 *130 - pakistan
 *132 - afganistan
 *134 - tadjikistan
 *136 - kirgizstan
 *138 - kazakhstan
 *140 - uzbekistan
 *142 - turkmenistan
 *144 - iran
 *146 - azerbajan
 *148 - turkey
 *150 - iraq
 *152 - syria
 *154 - saudi arabia
 *156 - oman
 *158 - yemen
 *160 - somalia
 *162 - ethiopia
 *164 - kenya
 *166 - uganda
 *168 - tanzania
 *170 - mozambique
 *172 - madagascar
 *174 - south africa
 *176 - lesotho
 *178 - namibia
 *180 - botswana
 *182 - zimbabwe
 *184 - zambia
 *186 - angola
 *188 - democratic congo
 *190 - congo
 *192 - gabon
 *194 - sudan
 *196 - C A R
 *198 - cam
 *200 - nigeria
 *202 - niger
 *204 - chad
 *206 - egypt
 *208 - libya
 *210 - algeria
 *212 - mali
 *214 - morocco
 *216 - western sahara
 *218 - mauritania
 *220 - senegal
 *222 - sierra leone
 *224 - ukraine
 *226 - belarus
 *228 - romania
 *230 - moldova
 *232 - bulgaria
 *234 - finland
 *236 - sweden
 *238 - norway
 *240 - united kingdom
 *242 - ireland
 *244 - portugal
 *246 - spain
 *248 - france
 *250 - germany
 *252 - poland
 *254 - czech
 *
 */

// 200 RED / 100 BLUE / VAR GREEN
/*
 *100 - lithuania
 *102 - latvia
 *104 - estonia
 *106 - italy
 *108 - hungary
 *110 - slovakia
 *112 - austria
 *114 - switzerland
 *116 - greece
 *118 - serbia
 *120 - bosnia
 *122 - slovenia
 *124 - belgium
 *126 - netherlands
 *128 - brazil
 *130 - bolivia
 *132 - paraguay
 *134 - argentina
 *136 - uruguay
 *138 - chile
 *140 - peru
 *142 - ecuador
 *144 - colombia
 *146 - venesuala
 *148 - guyana
 *150 - suriname
 *152 - french guyana
 *154 - mexico
 *156 - united states
 *158 - canada
 *160 - greenland
 *162 - iceland
 *164 - guatemala
 *166 - honduras
 *168 - nicaragua
 *170 - costa rica
 *172 - panama
 */