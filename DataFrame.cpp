//
// Created by fox on 3/21/20.
//

#include "DataFrame.h"


DataFrame::DataFrame() {
    df.clear();
    columns = 0;
    rows = 0;
    dfPartitionSeparator = 0;
}

bool DataFrame::universalCSVReader(std::string *buffer) {
    std::string path = "source/countries/";
    std::ifstream input1, input2;

    input1.open(path + "part1.csv", std::ios::in);
    if (!input1) {
        *buffer = "part1.csv";
        return false;
    }

    input2.open(path + "part2.csv", std::ios::in);
    if (!input2) {
        *buffer = "part2.csv";
        return false;
    }

    std::string temp;
    for (int i = 0; input1 >> temp; i++) {
        std::string data_cell, read_row;

        rows++;
        df.resize(rows);

        getline(input1, read_row);
        std::stringstream s_stream_input1(read_row);
        for (int r_exp = 0; getline(s_stream_input1, data_cell, ';'); r_exp++) {
            if (r_exp == 0) {
                data_cell.erase(std::remove_if(data_cell.begin(), data_cell.end(),
                                               [](auto const &c) -> bool { return c < 30 || c > 122; }),
                                data_cell.end());
                data_cell.erase(data_cell.begin());
            } else
                data_cell.erase(std::remove_if(data_cell.begin(), data_cell.end(),
                                               [](auto const &c) -> bool { return c < 40 || c > 122; }),
                                data_cell.end());

            df[i].resize(r_exp + 1);
            df[i][r_exp] = data_cell;
        }

        if (df[i].size() > columns)columns = df[i].size();
    }
    dfPartitionSeparator = rows;
    temp.clear();
    for (int i = rows; input2 >> temp; i++) {
        std::string data_cell, read_row;

        rows++;
        df.resize(rows);

        getline(input2, read_row);
        std::stringstream s_stream_input2(read_row);

        for (int r_exp = 0; getline(s_stream_input2, data_cell, ';'); r_exp++) {
            if (r_exp == 0) {
                data_cell.erase(std::remove_if(data_cell.begin(), data_cell.end(),
                                               [](auto const &c) -> bool { return c < 30 || c > 122; }),
                                data_cell.end());
                data_cell.erase(data_cell.begin());
            } else
                data_cell.erase(std::remove_if(data_cell.begin(), data_cell.end(),
                                               [](auto const &c) -> bool { return c < 40 || c > 122; }),
                                data_cell.end());

            df[i].resize(r_exp + 1);
            df[i][r_exp] = data_cell;
        }

        if (df[i].size() > columns)columns = df[i].size();

    }
    return true;
}

void DataFrame::showData() {
    for (int i = 0; i < rows; i++) {
        printf("%-5d", i);
        for (int g = 0; g < df[i].size(); g++) {
            printf("%-20s", df[i][g].c_str());
        }
        printf("\n");
    }
}

std::string DataFrame::getData(int exp_row, int exp_col) {
    if (exp_row < df.size())
        if (exp_col < df[exp_row].size())
            return df[exp_row][exp_col];
    return "";
}
