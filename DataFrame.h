//
// Created by fox on 3/21/20.
//

#ifndef PLAGUEINC_DATAFRAME_H
#define PLAGUEINC_DATAFRAME_H

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

class DataFrame{
private:
    std::vector<std::vector<std::string>> df;
    int columns, rows, dfPartitionSeparator;

public:
    DataFrame();
    bool universalCSVReader(std::string *buffer);
    void showData();
    std::string getData( int exp_row, int exp_col);
    std::vector<std::vector<std::string>> getFrame(){return df;}
};

#endif //PLAGUEINC_DATAFRAME_H
