//
// Created by fox on 2/24/20.
//
#ifndef PLAGUEINC_GRAPHICS_H
#define PLAGUEINC_GRAPHICS_H

#include <SFML/Graphics.hpp>
#include "DataFrame.h"
#include <fstream>
#include <string>

using namespace sf;

#define LOGO_NAME_SIZE_X 700
#define LOGO_NAME_SIZE_Y 97
#define LOGO_SIZE_X 362
#define LOGO_SIZE_Y 424

#define COUNTRIES_SIZE 100

#define STRINGS_OFFSET_X 20
#define STRINGS_OFFSET_Y 55

#define MAP_SCALE_FACTOR 0.825f
#define BAR_QUANTITY 3
#define INFO_QUANTITY 13
//----------------------------------------------------------------------------------------------------------------------
// MAIN SCREEN OBJECT
//----------------------------------------------------------------------------------------------------------------------
class MainScreen{
private:
    Image loadingName_i, loadingLogo_i;
    Texture loadingName_t, loadingLogo_t;
    Sprite loadingScreenName_s, loadingScreenLogo_s;
    double nameFade, logoFade;
public:
    MainScreen(RenderWindow &current_window);
    void updateVariables(double timePerFrame, double timeElapsed, RenderWindow &inputScreen);
    void draw(RenderWindow &inputScreen);
};
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------
// MAIN SCREEN BUTTONS
//----------------------------------------------------------------------------------------------------------------------

class MenuButtons {
private:
    const int dimension_size = 256;
    const int square_size = 8;
    const float gear_offset = 23.5;
    const float gear_scale_factor = 0.8f;
    const float gear_scale_main = 0.3f;

    Image square_i, joystick_i, gear_i, door_i, folder_i;
    Texture square_t, joystick_t, gear_t, door_t, folder_t;
    Sprite square_s[4], joystick_s, gear_s[2], door_s, folder_s;

    double gear_velocity, mainFade, timeOff;
    int updateVariables(double timePerFrame, double timeElapsed, RenderWindow &current_window);
    void draw(RenderWindow &current_window);

public:
    MenuButtons(RenderWindow &current_window);

    void offset(double x, double y);
    int process(double timePerFrame, double timeElapsed, RenderWindow &current_window);
};


class WorldMap{
private:
    Image map_mask, map_real_i, map_outline_i;
    Texture map_real_t, map_mask_t, map_outline_t;
    Sprite map_real_s, map_mask_s, map_outline_s;


    bool mapMode;
    double mapButtonOff, mapClickOff;

public:
    WorldMap(RenderWindow &current_window);

    void updateVariables(double timePerFrame, double timeElapsed, RenderWindow &current_window);
    void draw(RenderWindow &current_window);
};

class Gui{
private:
    Image InfoBar_i, statusBar_i;
    Texture InfoBar_t, statusBar_t;
    Sprite InfoBar_s[BAR_QUANTITY], statusBar_s;

    Font Info_f;
    Text Info_t[INFO_QUANTITY], statusText_t;

    int tempCountryID;

    std::string selectedCountry;

public:
    Gui(RenderWindow &current_window);

    void updateVariables(double timeElapsed, RenderWindow &current_window, DataFrame df);
    void draw(RenderWindow &current_window);

};



#endif //PLAGUEINC_GRAPHICS_H
