#include "SIR_model.h"
#include "Graphics.h"
#include "ErrorFox.h"

int main(int argc, char **argv) {
    RenderWindow mainWindow(VideoMode::getDesktopMode(), "PlagueInc by UTM");
    mainWindow.setVerticalSyncEnabled(true);
    mainWindow.setFramerateLimit(1000);
    Clock animationDelayClock, nonStopClock;

    //Main menu logo + title
    MainScreen mainLogo(mainWindow);
    //Main menu buttons
    MenuButtons mainButtons(mainWindow);
    //Game map
    WorldMap mainMap(mainWindow);
    //Game gui
    Gui mainGui(mainWindow);
    //Dataframe containing all statistics 
    DataFrame mainDataFrame;
    //Initialize error catching mechanism
    ErrorFox mainErrorHunter;


    std::string buffer;
    bool frameStatus = mainDataFrame.universalCSVReader(&buffer);
    bool gameStart = 0;


    if(frameStatus==0){
        mainWindow.close();
        mainErrorHunter.errorFoxReport(0, buffer);
        return 0;
    }

    while (mainWindow.isOpen()) {
        Event event;
        while (mainWindow.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                mainWindow.close();
        }

        //Estimated time to draw a frame
        float timePerFrame = animationDelayClock.getElapsedTime().asMicroseconds();
        timePerFrame /= 800;

        //Elapsed time since launch
        float timeElapsed = nonStopClock.getElapsedTime().asSeconds();

        animationDelayClock.restart();


        mainWindow.clear(Color(243,243,243));


        //Before the game has started (menu)
        if(!gameStart) {
            if (timeElapsed > 3)
                gameStart = mainButtons.process(timePerFrame, timeElapsed, mainWindow);

            mainLogo.updateVariables(timePerFrame, timeElapsed, mainWindow);
        }
        //After the game has started
        else {
            mainMap.updateVariables(timePerFrame, timeElapsed, mainWindow);
            mainGui.updateVariables(timeElapsed, mainWindow, mainDataFrame);
        }

        mainWindow.display();
    }
    return 0;
}
